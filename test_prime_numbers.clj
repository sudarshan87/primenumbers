(deftest test-get-first-n-primes
  (testing "getting the first n primes"
    (is (= [] (get-first-n-primes -1)))
    (is (= [] (get-first-n-primes 0)))
    (is (= [2] (get-first-n-primes 1)))
    (is (= [2 3] (get-first-n-primes 2)))
(is (= [2 3 5 7 11] (get-first-n-primes 5)))))

(deftest prime?-test
  (testing "A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself."
    (is (= false (prime? -1)))
    (is (= false (prime? 0)))
    (is (= false (prime? 1)))
    (is (= true (prime? 2)))
    (is (= true (prime? 3)))
    (is (= false (prime? 4)))
    (is (= true (prime? 5)))
    (is (= false (prime? 6)))
    (is (= true (prime? 7)))
    (is (= true (prime? 31)))
    (is (= false (prime? 32)))))

(deftest test-multiplication-table
  (testing "printing the multiplication table"
    (is (= (with-out-str (-main "3"))
           (str "\n"
                "| * |  2 |  3 |  5 |\n"
                "|---+----+----+----|\n"
                "| 2 |  4 |  6 | 10 |\n"
                "| 3 |  6 |  9 | 15 |\n"
                "| 5 | 10 | 15 | 25 |\n")))))
