(defn isPrime? [n]
    (let [divisors (range 2 (inc (int (Math/sqrt n)))) 
          remainders (map #(mod n %) divisors)]
          (not-any? #(= % 0) remainders)))
  
(defn multiplicator [n]
(println n)
(loop [x n]
(if (not (empty? x))
(do
(println (map #(* (first x) %) n))
(recur (rest x))
))))
  
(defn nPrimesCalculator []
  (println "Enter the limit")
  (let [limit (read-line)]
  (->> (range 2 (inc (Integer/parseInt limit)))
             (filter isPrime?)
(multiplicator)
)))
